import gulp from 'gulp';
import plumber from 'gulp-plumber';
import notify from 'gulp-notify';
import sass from 'gulp-sass';
import watch from 'gulp-watch';
import livereload from 'gulp-livereload';

gulp.task('styles', () => watch('./src/styles/sass/**/*.sass', () => {
  gulp
    .src(['./src/styles/sass/app.scss', './src/styles/sass/vendors.scss'])
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(sass())
    .pipe(
      notify({
        title: 'Gulp',
        subtitle: 'success',
        message: 'Admin Sass Task',
        sound: 'Pop',
      }),
    )
    .pipe(livereload())
    .pipe(gulp.dest('./src/styles/css/'));
}));

gulp.task('watch', () => {
  livereload.listen();
  gulp.watch('./src/styles/sass/**/*.sass', ['styles']);
});

gulp.task('default', ['styles']);
