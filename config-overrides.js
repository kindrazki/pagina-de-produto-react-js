const rewireAliases = require('react-app-rewire-aliases');
const { paths } = require('react-app-rewired');
const path = require('path');

/* config-overrides.js */
module.exports = function override(config, env) {
  config = rewireAliases.aliasesOptions({
    '@components': path.resolve(__dirname, `${paths.appSrc}/app/components`),
    '@routes': path.resolve(__dirname, `${paths.appSrc}/routes`),
    '@duck': path.resolve(__dirname, `${paths.appSrc}/store/ducks`),
    '@services': path.resolve(__dirname, `${paths.appSrc}/services`),
    '@API': path.resolve(__dirname, `${paths.appSrc}/services/API/index.js`),
  })(config, env);
  return config;
};
