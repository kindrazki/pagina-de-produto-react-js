import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Routers from './routes/routers';
import Store from './store';

// importando css
import './styles/css/vendors.css';
import './styles/css/app.css';

ReactDOM.render(
  <Provider store={Store}>
    <Routers />
  </Provider>,
  document.getElementById('root'),
);
