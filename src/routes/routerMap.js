import Home from '../app';
import SingleProduct from '../app/pages/singleProduct';
import ListProduct from '../app/pages/listProd';
import Cart from '../app/pages/cart';

const AsyncRouteMap = {
	Home,
	SingleProduct,
	ListProduct,
	Cart
};

export default AsyncRouteMap;
