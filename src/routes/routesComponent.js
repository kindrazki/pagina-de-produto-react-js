import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import routerMap from './routerMap';

class RoutesComponent extends Component {
  render() {
    return (
			<Switch>
					<Route exact path="/" component={routerMap.Home} />
					<Route path="/produto/:productID/:textLink" component={routerMap.SingleProduct}/>
					<Route path="/busca/:textBusca" component={routerMap.ListProduct}/>
					<Route path="/carrinho" component={routerMap.Cart}/>
			</Switch>
    );
  }
}

export default RoutesComponent;
