import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import RoutesComponent from './routesComponent';

const RouterComponent = () => (
	<BrowserRouter>
			<RoutesComponent />
	</BrowserRouter>
);

export default RouterComponent;





