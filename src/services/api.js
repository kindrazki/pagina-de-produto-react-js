import { create } from 'apisauce';
import { url } from './links';

const api = create({
	baseURL: url,
	headers: {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
	}
});

export default api;
