import api from '@services/api';
import { LKS } from '@services/links';

/*
 ** Em situações onde eu precisaria armazenar o GET da API trataria
 ** as funções de inputComplete e fullText na Action do Redux;
 */

export const InputComplete = async (text) => {
  if (!text) return false;

  let json;

  await api
    .get(LKS.autocomplete, {
      productNameContains: text,
    })
    .then(response => (json = response.data.itemsReturned))
    .catch(error => console.log(error));

  return json;
};

export const FullText = async (text) => {
  if (!text) return false;

  let json;

  await api
    .get(LKS.search + text, {
			map: 'ft',
			_from: 1,
			_to: 3
    })
    .then(response => (json = response.data))
		.catch(error => console.log(error));

  return json;
};


export const SingleProduct = async id => {
		if (!id) return false;

		let json;

		await api
			.get(LKS.search, {
				fq: `productId:${id}`,

			})
			.then(response => (json = response.data))
			.catch(error => console.log(error));
		return json;
}

export const ListProduct = async text => {
	if (!text) return false;

  let json;

  await api
    .get(LKS.search + text, {
			map: 'ft',
    })
    .then(response => (json = response.data))
		.catch(error => console.log(error));
		console.log(json)

	return json;
}

