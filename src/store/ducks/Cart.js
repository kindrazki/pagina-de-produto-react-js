import {createActions, createReducer} from 'reduxsauce';

const _ = require('lodash');

export const { Types, Creators } = createActions({
	addCart: ['product'],
	cartQuantity:['product'],
	removeCart: ['product']
})

const INITIAL_STATE = [];


const addCart = (state = INITIAL_STATE, action) => [...state, {
		productId: action.product.productId,
		productName: action.product.productName,
		brand: action.product.brand,
		image: action.product.items[0].images[0].imageUrl,
		price: action.product.items[0].sellers[0].commertialOffer.PriceWithoutDiscount,
		quantity: 1
}]


const addQuantityCart = (state = {}, action) => {
		let cart = state;
		if (_.find(cart, ['productId', action.product.productId]) !== undefined) {
			var index = _.findIndex(cart, function(o) {
				return o.productId === action.product.productId;
			});
			cart[index].quantity = ++cart[index].quantity;
			cart[index].price = ++cart[index].price;
		}

		return cart;
}

const removeToCart = (state = INITIAL_STATE, action) =>  {
	return Object.assign([], state.filter(item => {
		if(item.productId === action.product.productId && item.quantity !== 1) {
			item.quantity -= 1;
		} else {
			return item.productId !== action.product.productId
		}
		return item;
	}))



}

export default createReducer(INITIAL_STATE, {
	[Types.ADD_CART] : addCart,
	[Types.CART_QUANTITY] : addQuantityCart,
	[Types.REMOVE_CART] : removeToCart

})
