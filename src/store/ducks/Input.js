import {createActions, createReducer} from 'reduxsauce';

export const { Types, Creators } = createActions({
	inputActive: ['status']
})

const INITIAL_STATE = false;

const inputBefore = (state = INITIAL_STATE, action) => action.status

export default createReducer(INITIAL_STATE, {
	[Types.INPUT_ACTIVE] : inputBefore
})
