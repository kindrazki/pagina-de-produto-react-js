import { combineReducers } from 'redux';

import Cart from './Cart';
import Input from './Input';

export default combineReducers({
	Cart,
	Input
});

