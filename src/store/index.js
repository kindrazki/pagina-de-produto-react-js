import { applyMiddleware, createStore, compose } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import history from '@routes/history';
import rootReducer from '@duck';

const middlewares = [routerMiddleware(history), thunk];

// Utilizado para Debug de Actions e Reducers do Redux
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(connectRouter(history)(rootReducer), composeEnhancers(applyMiddleware(...middlewares)));

export default store;
