import React, { Component } from 'react';
import { SingleProduct } from '@API';
import { Header, CardProd } from '@components';
import Loading from '../../images/loading.gif';

class pages extends Component {
	constructor(props) {
		super(props);
		this.state = {
			product: [],
			image: null,
			price: null,
			load: true,
		};
	}
	componentDidMount() {
		this.RestAPI();
	}

	componentDidUpdate() {
		this.RestAPI();
	}

	RestAPI = async () => {
		const { productID } = this.props.match.params;

		const reponseProduct = await SingleProduct(productID);

		this.setState({
			load: false,
			product: reponseProduct[0],
			image: reponseProduct[0].items[0].images[0].imageUrl,
			price: reponseProduct[0].items[0].sellers[0].commertialOffer.PriceWithoutDiscount,
		});
	};

	render() {
		const { product, image, price, load } = this.state;
		return (
			<React.Fragment>
				<Header />
				<div className="container mt-5">
					{load ? (
						<div className="w-100 hh-100 d-flex justify-content-center align-items-center">
							<img src={Loading} alt="" />
						</div>
					) : (
						<React.Fragment>
							<CardProd product={product} image={image} price={price} />
						</React.Fragment>
					)}
				</div>
			</React.Fragment>
		);
	}
}

export default pages;
