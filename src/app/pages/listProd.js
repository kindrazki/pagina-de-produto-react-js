import React, { Component } from 'react';
import { ListProduct } from '@API'
import { Header, ListProdCard } from '@components';
import Loading from '../../images/loading.gif';


class ListProd extends Component {
	constructor(props){
		super(props)
		this.state = {
			product: [],
			recent: '',
			load: true
		}
	}

	componentDidMount() {
			this.RestAPI()
		}

	componentDidUpdate() {
			const { textBusca } = this.props.match.params
			const { recent } = this.state

			if(textBusca === recent) return false;

			this.RestAPI()
	}


	RestAPI = async () => {
		const { textBusca } = this.props.match.params

		const reponseProduct = await ListProduct(textBusca)

		this.setState({
			load: false,
			product: reponseProduct,
			recent: textBusca,
		})
	}

	render() {

		const { product, load } = this.state
		return (
			<React.Fragment>
				<Header />
				<div className="container mt-5">
					{load ? (
						<div className="w-100 hh-100 d-flex justify-content-center align-items-center">
							<img src={Loading} alt="" />
						</div>
					) : (
						<div className="row result_search_list">
							{product.map((item, index) => <ListProdCard key={index} product={item} />)}
						</div>
					)}
				</div>
			</React.Fragment>
		);
	}
}

export default ListProd;

