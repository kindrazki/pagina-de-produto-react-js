import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, CartProd } from '@components';
import { bindActionCreators } from 'redux';
import { Creators as CardActions } from '@duck/Cart'


class CartComponent extends Component {
	constructor(props){
		super(props)
		this.state = {
			product: [],
			recent: ''
		}
	}

	handleCard(e) {
		console.log(e)
	}

	render() {
		const { cart } = this.props
		return (
			<React.Fragment>
				<Header />
				<div className="container mt-5">
					<div className="row result_search_list">
						{cart.length ? <CartProd product={cart} /> : <div> Nenhum produto no carrinho </div>}
					</div>
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => ({
	cart: state.Cart
});

const mapDispatchToProps = dispatch => bindActionCreators(CardActions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(CartComponent);

