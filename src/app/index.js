
import React, { Component } from 'react';
import { Form } from '@components';

export default class app extends Component {

	componentDidMount(){
		var height = document.querySelectorAll('body')[0].offsetHeight
		height = (height / 2) - 80

		document.documentElement.style.setProperty('--input-animated', ` -${height}px`);
	}
	render() {
		return (
			<div className="d-flex justify-content-center align-items-center">
				<Form />
			</div>
		);
	}
}
