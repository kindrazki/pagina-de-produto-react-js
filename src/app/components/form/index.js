import React, { Component } from 'react';
import { Input, ResultInput } from '@components';
import { InputComplete, FullText } from '@API';
import { withRouter } from 'react-router-dom';


class header extends Component {

	constructor(props) {
		super(props)
		this.state = {
			partial: [],
			prod: [],
			searchText: '',
			blur: false
		}
	}

	componentWillUnmount() {
		this.setState({
			partial: [],
			prod: [],
			searchText: '',
			blur: false
		})
	}

	restAPI = async text => {


		this.setState({
			searchText: text.toLowerCase()
		})

		let timeout = 0;

		const { searchText } = this.state
		if(searchText.length === 0 || searchText.length === 1) {
			this.setState({
				prod: [],
				partial: []
			})
		}

		if(timeout) return clearTimeout(timeout);
		timeout = setTimeout( async () => {
			let responseFullText = await FullText(searchText);
			this.setState({
				prod: responseFullText
			})
		}, 1000);

		if(searchText.length < 1) return false;

		let responseInputComplete = await InputComplete(searchText)
		this.setState({
			partial: responseInputComplete
		})

	}

	render() {
		const { partial, prod, searchText } = this.state
		const { pathname } = this.props.location
		return (
			<div className="header-middle">
				<div className={pathname === '/' ? 'container d-flex justify-content-center align-items-center h-100' : ''}>
					<div className="input">
						<Input
							placeholde="O que você deseja buscar?"
							keyUp={(e) => {this.restAPI(e.target.value)}}
							content= {<ResultInput partial={partial} prod={prod} term={searchText}/>}
						/>
					</div>
				</div>
			</div>
		)
	}
}


export default withRouter(header);
