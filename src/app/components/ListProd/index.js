import React from 'react';
import { Link  } from 'react-router-dom';

const priceFormat =  (value) => {
	return `R$ ${value
		.toFixed(2)
		.replace(/\./g, ',')
		.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
}


const ListProdCard = ({product}) => {
	return (
		<Link
			to={`/produto/${product.productId}/${product.linkText}`}
			className="card_search col-md-3 mb-4"
		>
			<span style={{ backgroundImage: `url(${product.items[0].images[0].imageUrl})` }} />
			<div className="desc">
				<div className="title">
					{product.productName.split('ø')[0].split('Ø')[0]}
				</div>
				<b className="price">
					{priceFormat(product.items[0].sellers[0].commertialOffer.PriceWithoutDiscount)}
				</b>
				<div className="button">
					<div className="btn link_button">Ver mais..</div>
				</div>
			</div>
		</Link>
	)
};

export default ListProdCard;
