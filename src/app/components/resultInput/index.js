import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames'
import { bindActionCreators } from 'redux';
import { Creators as InputActions } from '@duck/Input'
import { Link ,withRouter } from 'react-router-dom';


const formatTermSearch = (label, term) => {
  if (!term) {
    return label;
  }
  return (
    <span>
      {label.split(term).reduce((prev, current, i) => {
				if (!i) {
					return [current];
				}
				return prev.concat(
					<span className="current_search" key={term + current}>
						{term}
					</span>,
					current.split('ø')[0],
				);
      }, [])}
    </span>
  );
};

const priceFormat =  (value) => {
	return `R$ ${value
		.toFixed(2)
		.replace(/\./g, ',')
		.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
}

class resultInput extends Component {

	constructor(props){
		super(props)
		this.state = {
			exit: false
		}
	}

	componentWillUnmount() {
		this.setState({
			exit: false
		})
	}

	render() {
		const { partial, term, prod, exit, inputActive } = this.props
		return (
				<React.Fragment>
					{partial === undefined || partial === null ? (
						<div className="justify-content-center align-items-center result_input">
								<div className="to_be">
								Hmmm... Parece que você esqueceu de ler o Readme.md <br></br>
								do repositório. Desative o CORS do navegador e tente <br></br>
								Novamente..
								</div>
						</div>
					) : (
						<div
							onMouseLeave={() => {this.setState({
								exit: true
							})}}
							className={classNames({
								'result_input': true,
								'justify-content-center align-items-center': !partial.length,
								'disabled': exit
							})}>
							{!partial.length ? (
								<div className="to_be">Continue digitando =D</div>
							) : (
								<React.Fragment>
									<div className="partial">
										<h2>Você quiz dizer: </h2>
										<ul>
											{partial
												&& partial.map((item, index) => {
													if (index >= 10) return false;
													return (
														<li key={index} className="search">
															<Link to={`/busca/${item.name}`} onClick={() => inputActive(false)}>
																{formatTermSearch(item.name, term)}
															</Link>
														</li>
													);
												})}
										</ul>
									</div>
									<div className="fullText">
										{prod.length ? <h2>Produtos sugeridos: </h2> : ''}
										<div className="result_search row m-0 p-0">
											{prod
												&& prod.map((item, index) => (
													<Link
														to={`/produto/${item.productId}/${item.linkText}`}
														key={index}
														onClick={() => inputActive(false)}
														className="card_search col-md-4 m-0 p-0"
													>
														<span style={{ backgroundImage: `url(${item.items[0].images[0].imageUrl})` }} />
														<div className="desc">
															<div className="title">
																{item.productName.split('ø')[0].split('Ø')[0]}
															</div>
															<b className="price">
																{priceFormat(item.items[0].sellers[0].commertialOffer.PriceWithoutDiscount)}
															</b>
														</div>
													</Link>
												))}
										</div>
									</div>
								</React.Fragment>
							)}
						</div>
					)}
				</React.Fragment>
		);
	}
}

const renderInput = withRouter(resultInput);

const mapDispatchToProps = dispatch =>  bindActionCreators(InputActions, dispatch);

export default connect(null,mapDispatchToProps)(renderInput);
