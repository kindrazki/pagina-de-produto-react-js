import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as CardActions } from '@duck/Cart'


const priceFormat =  (value) => {
	return `R$ ${value
		.toFixed(2)
		.replace(/\./g, ',')
		.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
}


const ListProdCart = ({product, removeCart}) => {
	return (
		<React.Fragment>
				{product.map((item,index) => {
					return(
							<div
								key={index}
								to={`/produto/${item.productId}/${item.productName}`}
								className="card_search col-md-3 mb-4"
							>
								<span style={{ backgroundImage: `url(${item.image})` }} />
								<div className="desc">
									<div className="title">
										{item.productName}
									</div>
									<b className="price">
										{priceFormat(item.price)}
									</b>
									<small className="qtd">quantidade : <b>{item.quantity}</b></small>
									<div className="button">
										<a href="#!"
											onClick={() => removeCart(item)}
											className="btn link_button red">Remover</a>
									</div>
								</div>
							</div>
					)
				})}
		</React.Fragment>
	)
};

const mapDispatchToProps = dispatch => bindActionCreators(CardActions, dispatch);

export default connect(null,mapDispatchToProps)(ListProdCart);

