import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Creators as CardActions } from '@duck/Cart'
const _ = require('lodash');

const priceFormat =  (value) => {
	return `R$ ${value
		.toFixed(2)
		.replace(/\./g, ',')
		.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
}

class cardSingle extends Component {

adicionarCarrinho = (product) => {
		var cart = this.props.cart

		if (_.find(cart, ['productId', product.productId]) !== undefined) {
			return this.props.cartQuantity(product)
		} else {
			return this.props.addCart(product)
		}

	}
	render() {
		const { product, image, price } = this.props
		return (
		<React.Fragment>
			{product.length ? (
				<div>espera</div>
			): (
				<div className="card_single row">
					<div className="image_full col-md-6">
					<img src={image} alt=""/>
					</div>
					<div className="content col-md-6">
						<h1 className="title">
							{product.productTitle}
						</h1>
						<div className="specific">
							<small>Cód( {product.productId} )</small>
							<small>em: {product.brand}</small>
						</div>
						<div className="price">
							{price !== null && priceFormat(price)}
						</div>
						<div className="button">
							<a href="#!" onClick={() =>{this.adicionarCarrinho(product)}} className="btn link_button">Adicionar ao carrinho</a>
						</div>
					</div>
					<div className="desc">
						<h2>Descrição do produto</h2>
						<p>
							{product.description}
						</p>
					</div>
				</div>
			)

			}
		</React.Fragment>
		);
	}
}


const mapStateToProps = state => ({
		cart: state.Cart
});

const mapDispatchToProps = dispatch => bindActionCreators(CardActions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(cardSingle);
