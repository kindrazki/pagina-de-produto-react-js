import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as InputActions } from '@duck/Input'
import classNames from 'classnames';

class inputSearch extends Component {

	render() {
		const { inputActive , actived } = this.props
		return (
				<div
					className={classNames({
						"inputSearch": true,
						"inputActive": actived
					})}
					onFocus={() => inputActive(true)}
					onBlur={() => inputActive(false)}
				>
					<input
						className="src-input"
						type="text"
						name="conteudo"
						placeholder="O que você deseja buscar?"
						autoComplete="off"
						tabIndex="2"
						pattern=".{1,}"
						required
						onKeyUp={this.props.keyUp}
					/>
					<button onClick={this.props.search}>
						<svg id="bhf_icon-search" viewBox="0 0 23 23"><path d="M15.7 15C17.1 13.4 18 11.3 18 9 18 4 14 0 9 0 4 0 0 4 0 9 0 14 4 18 9 18 11.3 18 13.4 17.1 15 15.7L22 22.7 22.7 22 15.7 15 15.7 15ZM9 17C4.6 17 1 13.4 1 9 1 4.6 4.6 1 9 1 13.4 1 17 4.6 17 9 17 13.4 13.4 17 9 17L9 17Z"></path></svg>
					</button>
					{this.props.content}
				</div>
		);
	}
}

const mapStateToProps = state => ({
	actived: state.Input
});

const mapDispatchToProps = dispatch => bindActionCreators(InputActions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(inputSearch);
