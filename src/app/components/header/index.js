import React, { Component } from 'react';
import { Form } from '@components';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';


class header extends Component {
	render() {
		const { card } = this.props
		return (
			<div className="header_product">
				<div className="container d-flex justify-content-between align-items-center">
				<Link to="/" className="logo">
					<img src="https://www.bluefoot.com.br/wp-content/uploads/2018/04/logo.png" alt="logo da bluefoot"/>
				</Link>
					<Form cardProd={card}/>
					<Link to="/carrinho" className="cart">
						<small className="qtd">
							{card}
						</small>
					</Link>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	card: state.Cart.length
});


export default connect(mapStateToProps)(header);


