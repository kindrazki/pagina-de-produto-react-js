
## Search semelhante ao Submarino 2019


### `Dependências`

Neste projeto foi utilizado o Boostrap 4 para monstagem do grid,<br>
Bibloteca redux com o padrão Duck pattern utilizando Reduxsauce<br>
Configurado também Gulp para Leitura e compilação do SASS <br>
ESLint padrão Airbnb para padronização dos códigos modelo ES6 <br>
React app rewired utilizado para gravação de aliases sem a necessidade de
exportar o projeto com **NPM RUN EJECT** <br>

As demais dependências, foram utilizadas apenas como aúxilio deste projeto.<br>

### `ANTES DE MAIS NADA`

**Não rode ainda o projeto pois ele dará erro!** <br>
 Isto por que a API que está por trás é baseada em `CORS` (Cross-Origin Resource Sharing) então, não vai funcionar local.


 > legal, não entendi nada  `=S`

**Tabom, vou explicar!** <br> os navegadores fazem uso de uma funcionalidade de segurança chamada Same-Origin Policy: um recurso de um site só pode ser chamado por outro site se os 2 sites estiverem sob o mesmo domínio (mesmo endereço, por ex.: meudominio.com.br). Isso limita a chamada de APIs REST por aplicações JS, por exemplo, hospedadas em servidores diferentes (front-end e back-end em camadas distintas). Isso porque o navegador considera recursos do mesmo domínio somente aqueles que usam o mesmo protocolo (http ou https), a mesma porta e o mesmo endereço (mesmo subdomínios, subdominio.meudominio.com.br, por exemplo, não são considerados seguros e não funcionam).

 **BELEZA, mais o que eu faço?** <br>

Simples :smile:, desative o `CORS` do seu navegador com o Código abaixo!<br>


## No OSX, abrir o Terminal e executar:
`$ open -a Google\ Chrome --args --disable-web-security --user-data-dir`

[--user-data-dir required on Chrome 49+ on OSX](https://stackoverflow.com/a/35509189/773263)

## Para Linux, executar:
`$ google-chrome --disable-web-security`

Além disso, se você estiver tentando acessar arquivos locais para fins de desenvolvimento, como AJAX ou JSON, também poderá
usar essa flag:
`-–allow-file-access-from-files`

## Para o Windows, vá para o Prompt de Comando, vá para a pasta em que o Chrome.exe está e digite:
`chrome.exe --disable-web-security`


****

### `Vamos para o que Interessa`

### `npm install && npm start` mas, eu prefiro YARN :innocent:


Para abrir em modo desenvolvimento <br>
Entre em [http://localhost:3000](http://localhost:3000) para visualizá-lo no navegador.

A página será recarregada se você fizer edições. <br>
Você também verá quaisquer erros de lint no console.

**`Super interessante:`** No meu projeto, configurei o Redux Devtools, então você pode acompanhar as chamadas..

Perai? não tem Redux Devtools? cara, onde se vai meu? baixa ai no link abaixo ;)

[https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=pt-BR](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=pt-BR)


**SHOW** Depois disto é só abrir o console e ta lá, para a sua alegria <3


 ****

 ### `Sobre o Projeto`

 Um Projeto bem simples mas que já tem um **V2** com API en Node JS em progresso, pois, aqui você pode adicionar todos os seus conhecimentos em Front - end e aprender cada vez mais com React e Redux e as de mais LIB's existentes neste mundo.





